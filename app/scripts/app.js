// $(".cboxElement").colorbox({href: 'https://www.youtube.com/embed/t5tAvDt_VFE?modestbranding=1;autoplay=1;controls=1;showinfo=0;rel=0;fs=1'});

$('.cboxElement').colorbox({href: 'https://www.youtube.com/embed/t5tAvDt_VFE'});

// $.colorbox({ href: 'http://www.youtube.com/embed/eh-0knDpn5g', width: '600px', height: '400px', iframe: true });

jQuery(document).ready(function($){
	//Examples of how to assign the ColorBox event to elements
	$('.colorbox').colorbox({iframe:true, innerWidth:480, innerHeight:390});
});

function getAllPost(){
	$.ajax({
		method: 'GET',
		url: url.all,
		success: function(e){
			NewsToArray(e);
		},
		error: function(e){
			console.log(e)
		}
	})
}
function NewsToArray(data){
	for (var i = 0; i < data.length; i++) {
		var yyyy = data[i].date.substr(0,4);
		var mm = renderDate('month', data[i].date.substr(5,2));
		var dd = renderDate('day', data[i].date.substr(8,2));

		if(data[i]._embedded['wp:featuredmedia']){
			var media = data[i]._embedded['wp:featuredmedia'][0].media_details.sizes.medium.source_url;
		}else{
			var media = ''
		}

		if (data[i].categories[0] == 1) {
			DATA_NEWS.push({
				title: data[i].title.rendered,
				date: dd + ' ' + mm + ' ' + yyyy,
				slug: data[i].slug,
				media: media,
				excerpt: data[i].excerpt.rendered,
				categories: data[i].categories[0]
			})

			DATA_NEWS_home.push({
				title: data[i].title.rendered,
				date: dd + ' ' + mm + ' ' + yyyy,
				slug: data[i].slug,
				media: media,
				excerpt: data[i].excerpt.rendered
			})
		} else {
			DATA_NEWS.push({
				title: data[i].title.rendered,
				date: dd + ' ' + mm + ' ' + yyyy,
				slug: data[i].slug,
				media: media,
				excerpt: data[i].excerpt.rendered,
				categories: data[i].categories[0]
			})
		}
		
		if(data.length - 1 === i) {
		    // console.log('loop ends');
		    $('.loading').hide();
		}
	}
	$('.loading').hide();
}

function renderDate(type, data){
	var d, m;
	if (type == 'month'){
		if (data == '01'){
			m = 'JAN';
		}
		else if (data == '02'){
			m = 'FEB';
		}
		else if (data == '03'){
			m = 'MAR';
		}
		else if (data == '04'){
			m = 'APR';
		}
		else if (data == '05'){
			m = 'MAY';
		}
		else if (data == '06'){
			m = 'JUNE';
		}
		else if (data == '07'){
			m = 'JULY';
		}
		else if (data == '08'){
			m = 'AUG';
		}
		else if (data == '09'){
			m = 'SEP';
		}
		else if (data == '10'){
			m = 'OCT';
		}
		else if (data == '11'){
			m = 'NOV';
		}
		else if (data == '12'){
			m = 'DEC';
		}

		return m;
	}
	else if (type == 'day'){
		if (data.substr(0,1) == '0'){
			d = data.substr(1,1);
		}
		else {
			d = data;
		}
		return d;
	}
}

jQuery.easing['jswing'] = jQuery.easing['swing'];
jQuery.extend( jQuery.easing,
{
	def: 'easeOutQuad',
	swing: function (x, t, b, c, d) {
		//alert(jQuery.easing.default);
		return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
	},
	easeInQuad: function (x, t, b, c, d) {
		return c*(t/=d)*t + b;
	},
	easeOutQuad: function (x, t, b, c, d) {
		return -c *(t/=d)*(t-2) + b;
	},
	easeInOutQuad: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	},
	easeInCubic: function (x, t, b, c, d) {
		return c*(t/=d)*t*t + b;
	},
	easeOutCubic: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t + 1) + b;
	},
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	},
	easeInQuart: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t + b;
	},
	easeOutQuart: function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	},
	easeInOutQuart: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
		return -c/2 * ((t-=2)*t*t*t - 2) + b;
	},
	easeInQuint: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t*t + b;
	},
	easeOutQuint: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t*t*t + 1) + b;
	},
	easeInOutQuint: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
		return c/2*((t-=2)*t*t*t*t + 2) + b;
	},
	easeInSine: function (x, t, b, c, d) {
		return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
	},
	easeOutSine: function (x, t, b, c, d) {
		return c * Math.sin(t/d * (Math.PI/2)) + b;
	},
	easeInOutSine: function (x, t, b, c, d) {
		return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
	},
	easeInExpo: function (x, t, b, c, d) {
		return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
	},
	easeOutExpo: function (x, t, b, c, d) {
		return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
	},
	easeInOutExpo: function (x, t, b, c, d) {
		if (t==0) return b;
		if (t==d) return b+c;
		if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
	},
	easeInCirc: function (x, t, b, c, d) {
		return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
	},
	easeOutCirc: function (x, t, b, c, d) {
		return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
	},
	easeInOutCirc: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
		return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
	},
	easeInElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
	},
	easeOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
	},
	easeInOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
		return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
	},
	easeInBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*(t/=d)*t*((s+1)*t - s) + b;
	},
	easeOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
	},
	easeInOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158; 
		if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
	},
	easeInBounce: function (x, t, b, c, d) {
		return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
	},
	easeOutBounce: function (x, t, b, c, d) {
		if ((t/=d) < (1/2.75)) {
			return c*(7.5625*t*t) + b;
		} else if (t < (2/2.75)) {
			return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
		} else if (t < (2.5/2.75)) {
			return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
		} else {
			return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
		}
	},
	easeInOutBounce: function (x, t, b, c, d) {
		if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
		return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
	}
});