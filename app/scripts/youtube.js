const API = 'AIzaSyBmTz96jy77IJNuphjGbqdEUq1CrBkDKfA';
const channelID = 'UCJcHKHamFsUu2V-Wkski2pA';
const result = 50;

var finalURL = `https://www.googleapis.com/youtube/v3/playlists?key=${API}&channelId=${channelID}&part=snippet,contentDetails&order=date&maxResults=${result}`

var playlistURL = `https://www.googleapis.com/youtube/v3/search?key=${API}&channelId=${channelID}&part=snippet,id&order=date&maxResults=${result}`

var DATA_VIDEO = [];

$.ajax({
	method: 'GET',
	url: finalURL,
	success: function(res){
		getVideos(res.items)
	},
	error: function(e){
		console.log(e)
	}
})

function getVideos(res){
	if(res[0]==undefined){
		console.log('kosong')
		window.location.reload();
	}else{
		res.map( (data, i) => {
			var snippet = data.snippet;
			// console.log(data.id)
			// console.log(data)
			// console.log(snippet.thumbnails.high)
			
			DATA_VIDEO.push({
				id: data.id,
				title: snippet.title,
				url: snippet.thumbnails.medium.url
			})
		})
	}
}

function getVideo(id, callback){
	// console.log('GET VIDEO CALLED: '+id)
	var title;
	DATA_VIDEO.map((data, i) => {
		if(id == data.id){
			// console.log(data.title)
			title = data.title;
		}else {}
	})
	var playlistid = id;
	$.ajax({
		method: 'GET',
		url: `https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=${playlistid}&key=${API}`,
		success: function(res){
			callback(title, res.items)
		},
		error: function(err){
			callback(err)
		}
	})
}