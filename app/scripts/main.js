var url = {
	'all' : 'http://wp.soekamti.com/wp-json/wp/v2/posts?per_page=100&_embed',
	'news' : 'http://wp.soekamti.com/wp-json/wp/v2/posts?categories=1&_embed'
}
var DATA_NEWS_home = [];
var DATA_NEWS = [];
var data = {
	'data_news_home' : DATA_NEWS_home,
	'data_news' : DATA_NEWS,
	'data_video' : DATA_VIDEO
}

$(document).ready(function(e){
	getAllPost();
	$('#navbar li').click(function(e){
		$(this).siblings().removeClass('active');
		$(this).addClass('active');

		setTimeout(function(e){
			var length = $('.news-container')[0].innerText.length;
			if (length > 100) {
				$('.loading').hide();
			}else{}
		}, 500)
	})
	$('#navbar li').click(function(e){
		console.log()
		if(!$(this).children()[1]){
			$('.navbar-toggle').removeClass('fa-times').addClass('fa-navicon');
		    $('.nav_wrapper').removeClass('active');
		    $('ul.dropdown-menu').removeClass('open');
		}else{}
	})
})

// Reuseable components
const Breadcrumb = Vue.component('breadcrumb', {
	props: ['title', 'pathname', 'desc'],
	template: '<section class="breadcrumb"><div class="container"><div class="row"><div class="col-md-6 col-sm-6 col-lg-6"><h1>{{title}}</h1><small class="text-grey text-uppercase">{{desc}}</small></div><div class="col-md-6 col-sm-6 col-lg-6"><ul><li><router-link to="/"><i class="fa fa-home"></i> Home</router-link></li><li>{{pathname}}</li></ul></div></div></div></section>'
})

const Breadcrumb_small = Vue.component('breadcrumb-small', {
	props: ['title', 'pathname', 'desc'],
	template: '<section class="breadcrumb breadcrumb-small"><div class="container"><div class="row"><div class="col-md-6 col-sm-6 col-lg-6"><h1 class="small">{{title}}</h1><small class="text-grey text-uppercase">{{desc}}</small></div><div class="col-md-6 col-sm-6 col-lg-6"><ul><li><router-link to="/"><i class="fa fa-home"></i> Home</router-link></li><li>{{pathname}}</li></ul></div></div></div></section>'
})

const SVGLoader = Vue.component('svgloader', {
	template: '<div class="loader loader--style2" title="1"> <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"> <path fill="#000" d="M25.251,6.461c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615V6.461z"> <animateTransform attributeType="xml"attributeName="transform"type="rotate"from="0 25 25"to="360 25 25"dur="0.6s"repeatCount="indefinite"/> </path> </svg> </div>'
})


// Pages components
const Home =  Vue.component('home-content', {
	data: function(e) {return data},
	template: '#contentHome'
})

const Gigs =  Vue.component('gigs-content', {
	data: function(e) {return data},
	template: '#contentGigs'
})

const News =  Vue.component('news-content', {
	data: function(e) {return data},
	template: '#contentNews'
})

const Contact =  Vue.component('contact-content', {
	template: '#contentContact'
})

const Biography =  Vue.component('biography-content', {
	template: '#contentBiography'
})

const Discography =  Vue.component('discography-content', {
	data: function(e) {return data},
	template: '#contentDiscography'
})

const Photo =  Vue.component('photo-content', {
	template: '#contentPhoto'
})

const Video =  Vue.component('video-content', {
	data: function(e) {return data},
	template: '#contentVideo'
})

const News_slug = {
	template: '#selectedNews',
	data: function(e){
		return {
			data: data,
			title: null,
			date: null,
			author: null,
			content: null
		};
	},
	created () {
		this.fetchData()
	},
	watch: {
		'$route' : 'fetchData'
	},
	methods: {
		fetchData () {
			getPost(this.$route.params.slug, (res) => {
				console.log(res)
				this.title = res.title.rendered;
				this.date = res.date;
				this.author = res.author;
				this.content = res.content.rendered;
			})
		}
	}
}

const Video_slug = {
	template: '#selectedVideo',
	data: function(e){
		return {
			data: data,
			title: null,
			items: []
		}
	},
	created() {
		this.fetchData()
	},
	watch: {
		'$route' : 'fetchData'
	},
	methods: {
		fetchData () {
			getVideo(this.$route.params.id, (title, res) => {
				this.title = title;
				res.map((data, i) => {
					var u = 'https://www.youtube.com/embed/'+data.snippet.resourceId.videoId;
					this.items.push({
						title: data.snippet.title,
						thumbnail: data.snippet.thumbnails.medium.url,
						url: u
					});
				})
				// console.log(this.title, this.items)
			})
		}
	}
}

const Photo_slug = {
	template: '#selectedPhoto',
	data: function(e){
		return {
			data: data,
			title: 'PHOTO'
		}
	},
	created() {
		this.fetchData()
	},
	watch: {
		'$route': 'fetchData'
	},
	methods: {
		fetchData () {
			getPhoto(this.$route.params.slug, (res) => {
				console.log(res)
			})
		}
	}
}


function getPost(slug, callback){
	console.log('GET POST CALLED: '+slug)
	$.ajax({
		method: 'GET',
		url: 'http://wp.soekamti.com/wp-json/wp/v2/posts?slug='+slug,
		success: function(res){
			callback(res[0])
		},
		error: function(err){
			callback(err)
		}
	})
}

function getPhoto(slug, callback){
	console.log(slug)
	callback('RESPONSE FROM GET PHOTO')
}

// Routes & Router
const routes = [
	{ path: '/', component: Home },
	{ path: '/gigs', component: Gigs },
	{ path: '/news', component: News },
	{ path: '/news/:slug', name: 'news', component: News_slug },
	{ path: '/contact', component: Contact },
	{ path: '/biography', component: Biography },
	{ path: '/discography', component: Discography },
	{ path: '/photo', component: Photo },
	{ path: '/photo/:slug', name: 'photo', component: Photo_slug },
	{ path: '/video', component: Video },
	{ path: '/video/:id', name: 'video', component: Video_slug }
]
const router = new VueRouter( 
	{
		mode: 'history',
		routes: routes
	}
)

// Render
const header = new Vue({
	el: '#header',
	router
})
const content_home = new Vue({
	el: '#content',
	router,
	data: function(e) {return data;}
})

$(document).on('click', 'a[href^="#"]', function(e) {
    // target element id
    var id = $(this).attr('href');

    if (id === '#'){
    	return;
    } else {
	    // target element
	    var $id = $(id);
	    if ($id.length === 0) {
	        return;
	    }

	    // prevent standard hash navigation (avoid blinking in IE)
	    e.preventDefault();

	    // top position relative to the document
	    var pos = $id.offset().top;

	    // animated top scrolling
	    $('body, html').animate({scrollTop: pos}, 1000, 'easeInOutExpo');
    }
});

(function($) {
 'use strict';
 $(window).resize(function() {
  var $ww = $(window).width();
  if ($ww <= 993) {
   $(document).on('click', '.yamm .dropdown-menu', function(e) {
    e.stopPropagation();
   });
   $('body').click(function(e) {
    if ($(e.target).closest('.navbar-default').length === 0) {
     $('.nav_wrapper').removeClass('active');
     $('ul.dropdown-menu').removeClass('open');
     $('.navbar-toggle').removeClass('fa-times').addClass('fa-navicon');
    }
   });
   $('.navbar-toggle').click(function(e) {
    e.stopImmediatePropagation();
    if ($(this).hasClass('fa-navicon')) {
     $(this).removeClass('fa-navicon').addClass('fa-times');
    } else {
     $(this).removeClass('fa-times').addClass('fa-navicon');
    }
    $('.nav_wrapper').toggleClass('active');
    $('ul.dropdown-menu').removeClass('open');
   });
   $('.navbar li.dropdown').click(function() {
    $('ul.dropdown-menu').removeClass('open');
    $(this).children('ul.dropdown-menu').toggleClass('open');
    $('.nav-level-down').fadeIn();
   });
   if ($('.nav-level-down').length == 0) {
    $('.navbar-default').find('ul.dropdown-menu').prepend('<li class="nav-level-down"><a class="nav-level-down" href="#" style="display: none;"><span class="fa fa-long-arrow-left"></span></a></li>');
   }
   $('.nav-level-down a').click(function(e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    $('ul.dropdown-menu').removeClass('open');
    $('.nav-level-down').fadeOut();
   });
  }
 }).resize();
 $('a.page-scroll').bind('click', function(event) {
  var $anchor = $(this);
  $('html, body').stop().animate({
   scrollTop: $($anchor.attr('href')).offset().top
  }, 1000, 'easeInOutExpo');
  event.preventDefault();
 });
 $('.navbar-nav a').click(function() {
  $('.navbar-nav > li').removeClass('active');
  $(this).parents('.navbar-nav > li').addClass('active');
 });
 $('.hash-menu').click(function(e) {
  e.preventDefault();
 });
 $('.colorbox-youtube').colorbox({
  iframe: true,
  innerWidth: 640,
  innerHeight: 360,
  maxWidth: '90%',
  maxHeight: '90%'
 });
 $('.colorbox-photo').colorbox({
  rel: 'album',
  maxWidth: '90%',
  maxHeight: '90%'
 });
 // $('.Collage').removeWhitespace().collagePlus().collageCaption();;
})(jQuery);